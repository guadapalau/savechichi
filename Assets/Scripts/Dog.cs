﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dog : MonoBehaviour {

    public int CantidadHuesos;
    //public Ui ui;

    void SumarHueso()
    {
        CantidadHuesos = CantidadHuesos + 1;
        //ui.MostrarHuesos(CantidadHuesos);
    }




    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

         transform.Translate(Input.GetAxis("Horizontal"),0,0);
        if (Input.GetKeyDown(KeyCode.A))
        {           
            GetComponent<Animator>().SetBool("CaminaIzquierda", true);
            GetComponent<Animator>().SetBool("CaminaDerecha", false);


        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            GetComponent<Animator>().SetBool("CaminaDerecha", true);
            GetComponent<Animator>().SetBool("CaminaIzquierda", false);


        }


        //transform.Translate(0, Input.GetAxis("Vertical"), 0);

    }

    private void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "hueso")
        {
            SumarHueso();
            Destroy(c.gameObject);
        }
    }

    private void OnTriggerEnter(Collider co)
    {
        if (co.tag == "hueso")
        {
            SumarHueso();
            Destroy(co.gameObject);
           
        }
    }

}
