﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventoTrigger : MonoBehaviour {

    public UnityEvent eventoAlHacerTrigger;

    private void OnTriggerEnter(Collider other)
    {
        eventoAlHacerTrigger.Invoke();
    }

   
}
